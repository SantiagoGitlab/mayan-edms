��          �   %   �      0     1     C     T     [     r     �     �     �     �  )   �  6   �          $     )  [   0  d   �     �     �          6     F     L     a  �  u  $   B  "   g     �  (   �  ,   �  
   �  .   �  0   !  !   R  @   t  d   �          )  
   0  [   ;  d   �     �  2   		  .   <	     k	     �	  (   �	  ,   �	                                                              	                                             
                    Create new groups Create new users Delete Delete existing groups Delete existing users Edit Edit existing groups Edit existing users Email Error deleting user "%(user)s": %(error)s Error reseting password for user "%(user)s": %(error)s Groups Name Submit Super user and staff user deleting is not allowed, use the admin interface for these cases. Super user and staff user password reseting is not allowed, use the admin interface for these cases. User User "%s" created successfully. User "%s" deleted successfully. User management Users View existing groups View existing users Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-16 22:48+0000
Last-Translator: Yaman Sanobar <yman.snober@gmail.com>
Language-Team: Arabic (http://www.transifex.com/rosarior/mayan-edms/language/ar/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 إنشاء مجموعات جديدة إنشاء مستخدمين جدد حذف حذف المجموعات الحالية حذف المستخدمين الحاليين تحرير تعديل المجموعات الموجودة تحرير المستخدمين الحاليين البريد الإلكتروني خطأ  أثناء حذف المستخدم "%(user)s": %(error)s خطأ أثناء إعادة تعيين كلمة المرور للمستخدم "%(user)s": %(error)s مجموعات اسم ارسال Super user and staff user deleting is not allowed, use the admin interface for these cases. Super user and staff user password reseting is not allowed, use the admin interface for these cases. مستخدم تم انشاء المستخدم "%s"  بنجاح. تم مسح المستخدم "%s"  بنجاح. إدارة المستخدم Users عرض المجموعات الحالية عرض المستخدمين الحاليين 